package br.com.dsw.service;

public abstract class AbstractObserver {

	private double temp;
	private double umidade;
	private double pressao;
	/* O construtor recebe o objeto ClimaDados (Subject) e o
	* usamos para registrar a exibição como um observador.
	*/
	private AbstractSubject subject;
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AbstractObserver)) return false; 

        //se forem o mesmo objeto, retorna true
        if(obj == this) return true;

        // aqui o cast é seguro por causa do teste feito acima
        AbstractObserver o = (AbstractObserver) obj; 

        //aqui você compara a seu gosto, o ideal é comparar atributo por atributo
        return this.temp == o.getTemp()  &&
                this.umidade == o.getUmidade();
    }   
	
	/**
	 * Método de atualização dos observadores
	 * @param temp
	 * @param umidade
	 * @param pressao
	 */
	public abstract void update(double temp, double umidade, double pressao);

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public double getUmidade() {
		return umidade;
	}

	public void setUmidade(double umidade) {
		this.umidade = umidade;
	}

	public double getPressao() {
		return pressao;
	}

	public void setPressao(double pressao) {
		this.pressao = pressao;
	}

	public AbstractSubject getSubject() {
		return subject;
	}

	public void setSubject(AbstractSubject subject) {
		this.subject = subject;
	}

}
