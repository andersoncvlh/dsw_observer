package br.com.dsw.service;

public interface AbstractSubject {

	/**
	 * registrar um novo observador
	 * @param o
	 */
	void registerObserver(AbstractObserver o);
	/**
	 * cancelar o registro de um observador
	 * @param o
	 */
	void unregisterObserver(AbstractObserver o);
	/**
	 * método para notificar todos os observadores
	 */
	void notifyObservers();
	
}
