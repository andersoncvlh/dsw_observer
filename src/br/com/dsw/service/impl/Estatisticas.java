package br.com.dsw.service.impl;

import java.util.List;

import br.com.dsw.service.AbstractObserver;
import br.com.dsw.service.AbstractSubject;
import br.com.dsw.service.Exibicao;

public class Estatisticas extends AbstractObserver implements Exibicao {

	private static final double TEMP_ONTEM = 33;
	
	public Estatisticas() {
		// TODO Auto-generated constructor stub
	}
	
	public Estatisticas (AbstractSubject climaDados) {
		this.setSubject(climaDados);
		getSubject().registerObserver(this);
	}

	@Override
	public void exibir() {
		String sensacao = (this.getTemp()>TEMP_ONTEM) ? "quente" : "frio";
		System.out.println("Estatisticas mostram: que o tempo hoje está mais "+sensacao+" do que ontem.");
		
	}

	@Override
	public void update(double temp, double umidade, double pressao) {
		this.setTemp(temp);
		this.setUmidade(umidade);
		this.setPressao(pressao);
		exibir();
	}

}
