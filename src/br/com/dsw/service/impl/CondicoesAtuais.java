package br.com.dsw.service.impl;

import br.com.dsw.service.AbstractObserver;
import br.com.dsw.service.AbstractSubject;
import br.com.dsw.service.Exibicao;

public class CondicoesAtuais extends AbstractObserver implements Exibicao {

	public CondicoesAtuais() {
		// TODO Auto-generated constructor stub
	}

	/**
	 *	O construtor recebe o objeto ClimaDados (Subject) e o
	 *	usamos para registrar a exibição como um observador
	 *	@param climaDados
	 */
	public CondicoesAtuais(AbstractSubject climaDados) {
		this.setSubject(climaDados);
		getSubject().registerObserver(this);
	}
	
	@Override
	public void update(double temp, double umidade, double pressao) {
		this.setTemp(temp);
		this.setUmidade(umidade);
		this.setPressao(pressao);
		exibir();
	}

	@Override
	public void exibir() {
		System.out.println("Condições atuais: " + getTemp() + "ºC e "+ getUmidade() + "% de umidade.");
	}
	
	
}
