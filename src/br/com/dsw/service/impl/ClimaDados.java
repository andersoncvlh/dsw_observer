package br.com.dsw.service.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.dsw.service.AbstractObserver;
import br.com.dsw.service.AbstractSubject;

public class ClimaDados implements AbstractSubject {

	private List<AbstractObserver> observers;
    private double temp;
    private double umidade;
    private double pressao;
	
	public ClimaDados() {
		observers = new ArrayList<AbstractObserver>();
	}

	@Override
	public void registerObserver(AbstractObserver o) {
		observers.add(o);
	}

	@Override
	public void unregisterObserver(AbstractObserver o) {
		if(observers.contains(o)){
			observers.remove(o);
		}
	}

	@Override
	public void notifyObservers() {
		for (AbstractObserver observer : observers) {
			System.out.println("Notificando os observadores!");
			observer.update(this.getTemp(),this.getUmidade(), this.getPressao());
		}
	}

	/*Aqui vamos apenas simular a Estação Metereológica. Podíamos
	pegar os dados da Web :)*/
	public void setMedidas (double t, double u, double p) {
		temp = t;
		umidade = u;
		pressao = p;
	}
	
	/**
	 * Getters & Setters
	 * @return
	 */
	
	public List<AbstractObserver> getObservers() {
		return observers;
	}

	public void setObservers(List<AbstractObserver> observers) {
		this.observers = observers;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public double getUmidade() {
		return umidade;
	}

	public void setUmidade(double umidade) {
		this.umidade = umidade;
	}

	public double getPressao() {
		return pressao;
	}

	public void setPressao(double pressao) {
		this.pressao = pressao;
	}

}
