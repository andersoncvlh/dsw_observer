package br.com.dsw.service.impl;

import br.com.dsw.service.AbstractObserver;
import br.com.dsw.service.AbstractSubject;
import br.com.dsw.service.Exibicao;

public class PrevisaoDoTempo extends AbstractObserver implements Exibicao {

	public PrevisaoDoTempo() {
		// TODO Auto-generated constructor stub
	}
	
	public PrevisaoDoTempo(AbstractSubject climaDados) {
		this.setSubject(climaDados);
		getSubject().registerObserver(this);
	}

	@Override
	public void exibir() {
		System.out.println("Previsão do tempo para proxima semana: "+ getTemp()*1.3%2f +"ºC e "+getUmidade()*0.7+"% de umidade.");
	}

	@Override
	public void update(double temp, double umidade, double pressao) {
		this.setTemp(temp);
		this.setUmidade(umidade);
		this.setPressao(pressao);
		exibir();
	}

}
