package br.com.dsw.app;

import br.com.dsw.service.impl.ClimaDados;
import br.com.dsw.service.impl.CondicoesAtuais;
import br.com.dsw.service.impl.Estatisticas;
import br.com.dsw.service.impl.PrevisaoDoTempo;

public class App {

	public static void main(String[] args) {
		//	inicia o Subject
		ClimaDados climaDados = new ClimaDados();
		//	inicia o Observer passando o subject por parâmetro e aqui já é feito o registro!
		CondicoesAtuais condicoesAtuais = new CondicoesAtuais(climaDados);
		PrevisaoDoTempo previsao = new PrevisaoDoTempo(climaDados);
		Estatisticas estatisticas = new Estatisticas(climaDados);
		//	define valores: temperatura, umidade, pressão
		climaDados.setMedidas(20, 2, 1);
		//	notifica todos os observers / exibe os valores
		climaDados.notifyObservers();
	}

}
